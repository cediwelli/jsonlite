# jsonlite

Lightweight JSON Parser.

## Building

Use `make` when you have installed all the Dependencies on your System.
Libraries do not have to be installed. It is possible to move the binaries into
the libs/ Directory and the headers into the include/ Directory.

## Usage

For a complete API overview, look at the json.h Header-File. The following
functions are an example for what the API looks like.

```c

// This function parses a String to the Object-Representation.
JSONObject * json_parse(char * str);

// Use like this:
char * strToJSON = "{\"test\": 0}";
JSONObject * myObject = json_parse(strToJSON);

// This function is used to get the Value of a named Key-Value-Pair from the Object.
int jsonobject_getByName(JSONObject * obj, char * name, JSONValue * valueOut);

// Use like this:
JSONValue value;
if (jsonobject_getByName(myObject, "test", &value) == 1) {
	printf("No found or other error.\n");
	return -1;
}

printf("Value: %d\n", value.val_long); // = 0

```

## Dependencies

### Libraries

- [datastructures-c](https://gitlab.com/cediwelli/datastructures-c)
