#include "json.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

#include <datastructures/buffer.h>

typedef struct {
	buffer * bs_buffer;
	stream * bs_stream;
} bufferedstream_t;

static uint32_t writeFile(uint64_t fd, uint8_t * buf, uint32_t n) {
	return fwrite(buf, sizeof(uint8_t), n, (FILE *)fd);
}

static int stringifyObject(JSONObject * object, bufferedstream_t * bufStream);
static int stringifyArray(JSONArray * object, bufferedstream_t * bufStream);

static int writeBuffer(bufferedstream_t * bufStream, char * bbuf, int n) {

	int rv = buffer_write(bufStream->bs_buffer, (uint8_t *) bbuf, n);

	if (buffer_getLength(bufStream->bs_buffer) >= 1024) {
		stream_transmit(bufStream->bs_stream, NULL);
		buffer_clear(bufStream->bs_buffer, BUFFER_CLEAR_READ);
	}
	return rv;
}

static int stringifyLong(JSONObject * object, bufferedstream_t * bufStream) {

	char tmpBuffer[256];
	int  n = 0;

	if (sprintf(tmpBuffer, "%ld%n", object->obj_valueLong, &n) < 1) {
		printf("long: sprintf failed.\n");
		return -1;
	}

	writeBuffer(bufStream, tmpBuffer, n);
	return 0;
}

static int stringifyDouble(JSONObject * object, bufferedstream_t * bufStream) {

	char tmpBuffer[256];
	int  n = 0;

	if (sprintf(tmpBuffer, "%lf%n", object->obj_valueDouble, &n) < 1) {
		printf("double: sprintf failed.\n");
		return -1;
	}

	writeBuffer(bufStream, tmpBuffer, n);
	return 0;
}

static int stringifyBool(JSONObject * object, bufferedstream_t * bufStream) {
	char * strTrue  = "true";
	char * strFalse = "false";

	if (object->obj_valueBool == 1) {
		writeBuffer(bufStream, strTrue, 4);
	} else {
		writeBuffer(bufStream, strFalse, 5);
	}
	return 0;
}

static int stringifyNull(JSONObject * object, bufferedstream_t * bufStream) {
	writeBuffer(bufStream, "null", 4);
	return 0;
}

static int stringifyString(JSONObject * object, bufferedstream_t * bufStream) {

	char * originalString = object->obj_valueString;
	int    escapedLen     = json_stringEscape(NULL, originalString);
	char   escapedString[escapedLen+1];

	json_stringEscape(escapedString, originalString);
	writeBuffer(bufStream, "\"", 1);
	writeBuffer(bufStream, escapedString, escapedLen);
	writeBuffer(bufStream, "\"", 1);

	return 0;
}

static int stringifyValue(JSONObject * object, bufferedstream_t * bufStream) {
	switch (object->obj_type) {
		case JSON_OBJECT:
			return stringifyObject(object, bufStream);
		case JSON_ARRAY:
			return stringifyArray(object, bufStream);
		case JSON_LONG:
			return stringifyLong(object, bufStream);
		case JSON_DOUBLE:
			return stringifyDouble(object, bufStream);
		case JSON_BOOL:
			return stringifyBool(object, bufStream);
		case JSON_NULL:
			return stringifyNull(object, bufStream);
		case JSON_STRING:
			return stringifyString(object, bufStream);
	}
	return -1;
}

static int stringifyField(JSONObject * object, bufferedstream_t * bufStream) {

	writeBuffer(bufStream, "\"", 1);
	writeBuffer(bufStream, object->obj_identifier, strlen(object->obj_identifier));
	writeBuffer(bufStream, "\":", 2);

	return stringifyValue(object, bufStream);
}

static int stringifyObject(JSONObject * object, bufferedstream_t * bufStream) {

	writeBuffer(bufStream, "{", 1);

	int childrenCount = array_getLength(object->obj_children);

	for (int i = 0; i < childrenCount; ++i) {

		JSONObject * childObject = array_get(object->obj_children, i);
		if (stringifyField(childObject, bufStream) == -1) {
			printf("object: field parsing went wrong!\n");
			return -1;
		}
		if (i < childrenCount-1) {
			writeBuffer(bufStream, ",", 1);
		}
	}

	writeBuffer(bufStream, "}", 1);
	return 0;
}

static int stringifyArray(JSONArray * object, bufferedstream_t * bufStream) {

	writeBuffer(bufStream, "[", 1);

	int childrenCount = array_getLength(object->obj_children);
	for (int i = 0; i < childrenCount; ++i) {

		JSONObject * childObject = array_get(object->obj_children, i);
		if (stringifyValue(childObject, bufStream) == -1) {
			printf("array: element parsing went wrong!\n");
			return -1;
		}
		if (i < childrenCount-1) {
			writeBuffer(bufStream, ",", 1);
		}
	}

	writeBuffer(bufStream, "]", 1);
	return 0;
}

char * json_stringify(JSONObject * object) {

	if (object != NULL) {
		buffer * strBuffer = buffer_create(BUFFER_MODE_TAPE);
		stream * s         = stream_create(1024);
		int      strLen    = 0;
		char *   strOut    = NULL;

		stream_addOut(s, (uint64_t)strBuffer, stream_toBuffer);

		if (json_stringifyToStream(object, s) == -1) {
			buffer_free(strBuffer);
			stream_free(s);
			return NULL;
		}

		strLen = buffer_getLength(strBuffer);
		strOut = malloc(strLen + 1);

		buffer_read(strBuffer, (uint8_t *)strOut, strLen);
		strOut[strLen] = '\0';

		buffer_free(strBuffer);
		stream_free(s);

		return strOut;
	}

	return NULL;
}

int json_stringifyToFile(JSONObject * object, char * filepath) {

	if (object != NULL && filepath != NULL) {

		FILE * file = fopen(filepath, "w+");
		stream * s  = NULL;

		if (file == NULL) {
			printf("stringifyToFile: Could not open file.\n");
			return -1;
		}

		s = stream_create(1024);
		stream_addOut(s, (uint64_t)file, writeFile);

		if (json_stringifyToStream(object, s) == -1) {
			stream_free(s);
			fclose(file);
			return -1;
		}

		fclose(file);
		stream_free(s);
		return 0;
	}
	return -1;
}

int json_stringifyToStream(JSONObject * object, stream * s) {

	if (s != NULL && object != NULL) {

		bufferedstream_t bufStream;
		buffer *         buffer = buffer_create(BUFFER_MODE_TAPE);

		stream_addIn(s, (uint64_t)buffer, stream_fromBuffer);

		bufStream.bs_buffer = buffer;
		bufStream.bs_stream = s;

		if (object->obj_type == JSON_OBJECT) {
			if (stringifyObject(object, &bufStream) == -1) {
				printf("stringifyToStream: stringifying object went wrong!\n");
				buffer_free(buffer);
				return -1;
			}
		} else if (object->obj_type == JSON_ARRAY) {
			if (stringifyArray(object, &bufStream) == -1) {
				printf("stringifyToStream: stringifying array went wrong!\n");
				buffer_free(buffer);
				return -1;
			}
		} else {
			buffer_free(buffer);
			return -1;
		}
		stream_transmit(s, NULL);
		buffer_free(buffer);
		return 0;
	}

	return -1;
}

int json_stringEscape(char * buffer, char * str) {

	if (str == NULL) {
		return -1;
	}

	int buflen = 0;
	for (int i = 0; str[i] != '\0'; ++i) {

		char c = str[i];

		char reserved_chars[] = {
			'\n', '\r', '\t', '\b', '\f', '\"', '\\'
		};

		char * escaped_chars[] = {
			"\\n", "\\r", "\\t", "\\b", "\\f", "\\\"", "\\\\"
		};

		int found = 0;
		for (int k = 0; k < sizeof(reserved_chars); ++k) {
			if (c == reserved_chars[k]) {
				if (buffer != NULL) {
					buffer[buflen+0] = escaped_chars[k][0];
					buffer[buflen+1] = escaped_chars[k][1];
				}

				buflen += 2;
				found = 1;
				break;
			}
		}

		if (!found) {
			if (buffer != NULL) {
				buffer[buflen] = c;
			}
			++buflen;
		}
	}

	if (buffer != NULL) {
		buffer[buflen] = '\0';
	}

	return buflen;
}

int json_stringUnescape(char * buffer, char * str) {

	int buflen = 0;
	for (int i = 0; str[i] != '\0'; ++i) {

		char c = str[i];

		if (c != '\\') {

			copy:
			if (buffer != NULL) {
				buffer[buflen] = c;
			}
			++buflen;

		} else {

			char escape_ctrl[] = {
				'n', 'r', 't', 'b', 'f', '\"', '\\'
			};

			char escape_repl[] = {
				'\n', '\r', '\t', '\b', '\f', '\"', '\\'
			};

			int found = 0;
			for (int k = 0; k < sizeof(escape_ctrl); ++k) {
				char c2 = str[i+1];
				if (c2 == escape_ctrl[k]) {

					if (buffer != NULL) {
						buffer[buflen] = escape_repl[k];
					}

					++i;
					++buflen;
					found = 1;
					break;
				}
			}

			if (!found) {
				// Probably unicode
				goto copy;
			}
		}
	}

	if (buffer != NULL) {
		buffer[buflen] = '\0';
	}
	return buflen;
}
