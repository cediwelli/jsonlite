#include "json.h"

#include <datastructures/buffer.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

typedef enum {
	JSTK_OBJOPEN, JSTK_OBJCLOSE, JSTK_ARROPEN, JSTK_ARRCLOSE, JSTK_OBJ, // OBJ == String, Number, Bool, ...
	JSTK_COLON, JSTK_COMMA, JSTK_STRING,
	JSTK_USED
} jsontokentype_e;

typedef struct {
	jsontokentype_e tk_type;
	void *          tk_payload;
} jsontoken_t;

static int parseObject(array * tokens, int startIndex, JSONObject ** obj_out);
static int parseArray(array * tokens, int startIndex, JSONArray ** arr_out);

static uint32_t readFile(uint64_t fd, uint8_t * buf, uint32_t n) {
	return fread(buf, sizeof(uint8_t), n, (FILE *)fd);
}

static void addToParent(JSONObject * parent, JSONObject * childObj) {
	if (parent->obj_type == JSON_OBJECT) {
		array_addSorted(parent->obj_children, childObj);
	} else {
		array_append(parent->obj_children, childObj);
	}
}

static int isNum(char c) {
	return ((c>='0')&&(c<='9')) || c=='-';
}

static int isWhite(char c) {
	return (c==' ') || (c=='\t') || (c=='\n') || (c=='\r');
}

static int skipWhite(char * str) {
	int i = 0;
	for (; isWhite(str[i]); ++i);
	return i;
}

static int findClosingQuote(char * str) {
	int quoteCount = 0;
	for (int i = 0; str[i] != '\0'; ++i) {
		if (str[i] == '\"') {
			if (str[i-1] != '\\') {
				quoteCount++;
				if (quoteCount == 2) {
					return i;
				}
			}
		}
	}
	return -1;
}

static int isFloatingPoint(char * str) {
	for (int i = 0; str[i] != '\0'; ++i) {
		if (str[i] == '.') {
			return 1;
		}
		if (!isNum(str[i])) {
			return 0;
		}
	}
	return 0;
}

static void freeObject(JSONObject * obj) {

	if (obj == NULL) {
		return;
	}
	if (obj->obj_identifier != NULL) {
		free(obj->obj_identifier);
		obj->obj_identifier = NULL;
	}

	switch (obj->obj_type) {
		case JSON_STRING: {
			if (obj->obj_valueString != NULL) {
				free(obj->obj_valueString);
				obj->obj_valueString = NULL;
			}
			break;
		}
		case JSON_ARRAY:
		case JSON_OBJECT: {
			if (obj->obj_children != NULL) {
				for (int i = 0; i < array_getLength(obj->obj_children); ++i) {
					freeObject(array_get(obj->obj_children, i));
				}
				array_free(obj->obj_children);
				obj->obj_children = NULL;
			}
			break;
		}
	}

	free(obj);
}

static void cleanupToken(jsontoken_t * tok) {
	/* This will automatically, by design, ignore all Tokens which were set */
	/* as USED.                                                             */
	switch (tok->tk_type) {
		case JSTK_OBJ: {
			freeObject(tok->tk_payload);
			tok->tk_type = JSTK_USED;
			break;
		}
		case JSTK_STRING: {
			free(tok->tk_payload);
			tok->tk_type = JSTK_USED;
			break;
		}
		default:
			break;
	}
}

static void cleanupTokens(array * tokens) {
	int len = array_getLength(tokens);
	for (int i = 0; i < len; ++i) {
		cleanupToken(array_get(tokens, i));
	}
}

static int testNull(char * str, JSONObject ** boolObj) {
	char nullString[]  = "null";

	if (strncmp(str, nullString, sizeof(nullString)-1) == 0) {
		JSONObject * tmpNullObj = malloc(sizeof(JSONObject));

		tmpNullObj->obj_type = JSON_NULL;
		*boolObj = tmpNullObj;
		return 4;
	}

	return -1;
}

static int testBool(char * str, JSONObject ** boolObj) {
	char trueString[]  = "true";
	char falseString[] = "false";

	if (strncmp(str, trueString, sizeof(trueString)-1) == 0) {
		JSONObject * tmpBoolObj = malloc(sizeof(JSONObject));

		tmpBoolObj->obj_type      = JSON_BOOL;
		tmpBoolObj->obj_valueBool = 1;
		*boolObj = tmpBoolObj;
		return 4;
	}

	if (strncmp(str, falseString, sizeof(falseString)-1) == 0) {
		JSONObject * tmpBoolObj = malloc(sizeof(JSONObject));

		tmpBoolObj->obj_type      = JSON_BOOL;
		tmpBoolObj->obj_valueBool = 0;
		*boolObj = tmpBoolObj;
		return 5;
	}

	return -1;
}

static int testNum(char * str, JSONObject ** numObj) {

	long   lval;
	double dval;
	int    n;
	int    rv;
	int    has_point = isFloatingPoint(str);

	if (!has_point) {
		rv = sscanf(str, "%ld%n", &lval, &n);
		if (rv == 1) {
			JSONObject * lngObj = malloc(sizeof(JSONObject));

			lngObj->obj_type      = JSON_LONG;
			lngObj->obj_valueLong = lval;
			*numObj = lngObj;
			return n;
		}

	} else {
		rv = sscanf(str, "%lf%n", &dval, &n);
		if (rv == 1) {
			JSONObject * dblObj = malloc(sizeof(JSONObject));

			dblObj->obj_type        = JSON_DOUBLE;
			dblObj->obj_valueDouble = dval;
			*numObj = dblObj;
			return n;
		}
	}

	return -1;
}

static int testString(char * str, char ** str_out) {

	int    idxClosingQuote = findClosingQuote(str);
	int    len             = idxClosingQuote - 1;
	char   bufferOriginal[len + 1];
	int    unescapedLen;
	char * bufferUnescaped;

	if (idxClosingQuote == -1) {
		return -1;
	}

	memcpy(bufferOriginal, &str[1], len);
	bufferOriginal[len] = '\0';

	unescapedLen    = json_stringUnescape(NULL, bufferOriginal);
	bufferUnescaped = malloc(unescapedLen + 1);

	json_stringUnescape(bufferUnescaped, bufferOriginal);
	bufferUnescaped[unescapedLen] = '\0';

	*str_out = bufferUnescaped;
	return len+2;
}

static int getNextToken(char * str, jsontoken_t * tok_out) {
	/* Will try to get the next Token in the String.           */
	/* These tokens may be anything listed in jsontokentype_e. */

	char startChar = str[0];
	//printf("getNextToken startChar %c@%d\n", startChar, startChar);

	/* Testing first for "un-payloaded" Tokens.       */
	/* These are just "control"-Tokens with no value. */
	switch (startChar) {
		case '{':
			tok_out->tk_type = JSTK_OBJOPEN;
			return 1;
		case '}':
			tok_out->tk_type = JSTK_OBJCLOSE;
			return 1;
		case '[':
			tok_out->tk_type = JSTK_ARROPEN;
			return 1;
		case ']':
			tok_out->tk_type = JSTK_ARRCLOSE;
			return 1;
		case ':':
			tok_out->tk_type = JSTK_COLON;
			return 1;
		case ',':
			tok_out->tk_type = JSTK_COMMA;
			return 1;
	}

	/* Checking if the Token is the NULL-Token. */
	if (startChar == 'n') {
		int          l       = 0;
		JSONObject * nullObj = NULL;

		/* Set Type and Payload for the Object.                         */
		/* There are less reduntand ways to set the Identifier to NULL. */
		if ((l = testNull(str, &nullObj)) > -1) {
			tok_out->tk_type        = JSTK_OBJ;
			tok_out->tk_payload     = nullObj;
			nullObj->obj_identifier = NULL;
			return l;
		}
	}

	/* Checking if the Token is a Boolean (true/false) */
	if (startChar == 't' || startChar == 'f') {
		int          l       = 0;
		JSONObject * boolObj = NULL;

		/* Set Type and Payload for the Object.                         */
		/* There are less reduntand ways to set the Identifier to NULL. */
		if ((l = testBool(str, &boolObj)) > -1) {
			tok_out->tk_type    = JSTK_OBJ;
			tok_out->tk_payload = boolObj;
			boolObj->obj_identifier = NULL;
			return l;
		}
	}

	/* If the first char is a Number, check for Numbers. */
	if (isNum(startChar)) {
		int          l       = 0;
		JSONObject * numObj  = NULL;

		/* Set Type and Payload for the Object.                         */
		/* There are less reduntand ways to set the Identifier to NULL. */
		if ((l = testNum(str, &numObj)) > -1) {
			tok_out->tk_type    = JSTK_OBJ;
			tok_out->tk_payload = numObj;
			numObj->obj_identifier = NULL;
			return l;
		}
	}

	/* Last Option. Check for String. */
	if (startChar == '\"') {
		char * strObj = NULL;
		int    l      = 0;

		/* Set Type and Payload for the Object.                         */
		/* There are less reduntand ways to set the Identifier to NULL. */
		if ((l = testString(str, &strObj)) > -1) {
			tok_out->tk_type    = JSTK_STRING;
			tok_out->tk_payload = strObj;
			return l;
		}
	}

	return -1;
}

static int tokenizer(buffer * dBuffer, stream * stream, array * tokens) {
	/* Tries to convert the source to tokens.                           */
	/* When it fails, the allocated memory is cleaned up automatically. */

	jsontoken_t currentToken;
	int         rv;
	int         index            = 0;
	int         currentBufferLen = 0;
	char *      str;

	/* We are using a Buffer to dynamically append more data to the */
	/* our input-String.                                            */
	while ((currentBufferLen = buffer_getLength(dBuffer)) > 0) {
		//printf("while tok\n");

		/* When we are using streaming Input and there is fewer Data left */
		/* than a String can be long, we want to poll more Data from the  */
		/* Stream. */
		if (stream != NULL && currentBufferLen < JSON_MAX_STRING_LENGTH) {
				stream_transmit(stream, NULL);
		}

		/* We are getting the underlying Buffer at the smallest unread Index */
		str = (char *) buffer_getBuffer(dBuffer);

		if (str == NULL) {
			break;
		}
		index += skipWhite(&str[index]);
		if (str[index] == '\0') {
			break;
		}

		/* We are reading the Tokens one-by-one. If the Tokens do not follow */
		/* the Grammar, we will abort reading and clear everything up. The   */
		/* incident must be reported to caller.                              */
		if ((rv = getNextToken(&str[index], &currentToken)) == -1) {
			printf("getNextToken failed %s, %d\n", &str[index], currentToken.tk_type);
			cleanupTokens(tokens);
			return -1;
		}
		index += rv;
		//printf("%d\n", currentToken.tk_type);

		/* Append read Token to Array. */
		array_append(tokens, &currentToken);

		/* We are skipping what we've just read and delete it, so that */
		/* the Buffer will not get too big.                            */
		buffer_skip(dBuffer, index);
		buffer_clear(dBuffer, BUFFER_CLEAR_READ);

		/* After clearing we have to reset the index to 0 to start from the */
		/* beginning again.                                                 */
		index = 0;
	}

	/* Everything went fine. */
	return 0;
}

static int tokenToObject(array * tokens, int index, char * identifier, JSONObject * parent) {
	/* Converts a Token to an JSONObject, when it is one. */

	jsontoken_t * token = array_get(tokens, index);

	/* If the token is classified as an OBJ, it already is a valid JSONObject */
	/* and just needs its Identifier.                                         */
	if (token->tk_type == JSTK_OBJ) {
		JSONObject * childObj    = token->tk_payload;
		childObj->obj_identifier = identifier;

		addToParent(parent, childObj);
		return 1;

	/* If the Object is a String, it has to be converted to an Object. For */
	/* that, we create a new JSONObject and take the String-Value from the */
	/* Token and shove it into the Object. */
	} else if (token->tk_type == JSTK_STRING) {
		JSONObject * childObj     = malloc(sizeof(JSONObject));
		childObj->obj_type        = JSON_STRING;
		childObj->obj_identifier  = identifier;
		childObj->obj_valueString = token->tk_payload;

		addToParent(parent, childObj);
		return 1;

	/* If we find an opening Brace, it might be a nested Object. This must  */
	/* be parsed recursively and therefor we are using parseObject.         */
	} else if (token->tk_type == JSTK_OBJOPEN) {
		JSONObject * childObj = NULL;
		int          rv       = parseObject(tokens, index, &childObj);

		if (rv != -1) {
			childObj->obj_identifier = identifier;

			addToParent(parent, childObj);
			return rv;
		}

	/* If there is an opening Bracket, there might be an Array, which has to */
	/* be parsed recursively, like the Object.                               */
	} else if (token->tk_type == JSTK_ARROPEN) {
		JSONArray * childArr = NULL;
		int         rv       = parseArray(tokens, index, &childArr);

		if (rv != -1) {
			childArr->obj_identifier = identifier;

			addToParent(parent, childArr);
			return rv;
		}
	}

	/* Nothing appropriate was found or parsing went wrong. Report that to */
	/* caller.                                                             */
	return -1;
}

static int parseField(array * tokens, int startIndex, JSONObject * parentObj) {
	/* Tries to parse a Field inside an Object                         */
	/* Makes use of tokenToObject and therefore adds the founds Object */
	/* directly to the parentObj.                                      */

	int           index = startIndex;
	jsontoken_t * currentToken;
	jsontoken_t * idToken;
	int           rv;

	/* Check if the Fields begins with a String. If not, report Error to */
	/* the caller.                                                       */
	idToken = array_get(tokens, index);
	if (idToken->tk_type != JSTK_STRING) {
		printf("No identifier\n");
		return -1;
	}
	++index;

	/* There must be a Colon, else it is a faulty Grammar which must be */
	/* reported to caller and parsing must be stopped.                  */
	currentToken = array_get(tokens, index);
	if (currentToken->tk_type != JSTK_COLON) {
		printf("No colon\n");
		return -1;
	}
	++index;

	/* Now parse the actual Value (whatever that may be).                  */
	/* The Value will be determined by tokenToObject and if the Value is a */
	/* valid JSONObject, it will be added to the parent.                   */
	currentToken = array_get(tokens, index);

	if ((rv = tokenToObject(tokens, index, idToken->tk_payload, parentObj)) == -1) {
		printf("Nothing\n");
		return -1;
	}

	/* We are proclaming the Tokens as used, so that they will not be cleared */
	/* afterwards.                                                            */
	currentToken->tk_type = JSTK_USED;
	idToken->tk_type      = JSTK_USED;

	/* Add two for Identifier and Colon. */
	return rv+2;
}

static int parseObject(array * tokens, int startIndex, JSONObject ** obj_out) {
	/* Tries to parse an Object from the Tokens. */

	int           index = 0;
	jsontoken_t * currentToken;
	JSONObject *  obj;

	/* Check if the first Token is really an opening Brace. */
	currentToken = array_get(tokens, index+startIndex);
	if (currentToken->tk_type != JSTK_OBJOPEN) {
		return -1;
	}
	++index;

	/* Create the Object so that the Fields may be added to the Object. */
	obj = malloc(sizeof(JSONObject));
	obj->obj_children   = array_create(0);
	obj->obj_identifier = NULL;
	obj->obj_type       = JSON_OBJECT;

	array_setCompMode(obj->obj_children, json_compare);

	/* Check if there is an early closing Brace. This will save time and */
	/* complexity. */
	currentToken = array_get(tokens, index+startIndex);
	if (currentToken->tk_type == JSTK_OBJCLOSE) {
		*obj_out = obj;
		return index + 1;
	}

	/* Parse the Fields. The Fields are automatically added to the Object in */
	/* parseField, because it makes use of tokenToObject which adds the      */
	/* Object directly to the Parent. */
	while (1) {

		/* Will take care of parsing the Identifier and Value. */
		int skip = parseField(tokens, index+startIndex, obj);

		/* Grammar Error was detected somewhere upwards of parseField. */
		/* Everything must be free'd */
		if (skip == -1) {
			printf("WRONG!\n");
			freeObject(obj);
			return -1;
		}
		index += skip;

		/* Check wheather the  Object expects more Data because of Comma or */
		/* the Object is done because of closing Brace. */
		currentToken = array_get(tokens, index+startIndex);
		if (currentToken->tk_type == JSTK_OBJCLOSE) {
			++index;
			break;
		} else if (currentToken->tk_type == JSTK_COMMA) {
			++index;
			continue;
		}

		/* If it is neither of the above, it is fauly Grammar and therefor */
		/* parsing must be terminated with no memory leaks. */
		freeObject(obj);
		return -1;
	}

	*obj_out = obj;
	return index;
}

static int parseArray(array * tokens, int startIndex, JSONArray ** arr_out) {
	/* Tries to parse an Array from the Tokens. */

	int           index = 0;
	jsontoken_t * currentToken;
	JSONArray *   arr;

	/* Check if the first Token is an opening Bracket */
	currentToken = array_get(tokens, index+startIndex);
	if (currentToken->tk_type != JSTK_ARROPEN) {
		printf("Array not opened! %d\n", currentToken->tk_type);
		return -1;
	}
	++index;

	/* Create the Array-Object, to allow later appending of Elements. */
	arr = malloc(sizeof(JSONArray));
	arr->obj_children   = array_create(0);
	arr->obj_identifier = NULL;
	arr->obj_type       = JSON_ARRAY;

	array_setCompMode(arr->obj_children, json_compare);

	/* This checks, wheather there is directly a closing Bracket. This */
	/* saves some time. */
	currentToken = array_get(tokens, index+startIndex);
	if (currentToken->tk_type == JSTK_ARRCLOSE) {
		*arr_out = arr;
		return index + 1;
	}

	/* Now we are parsing the Elements of the Array. They will be read and  */
	/* added to the array by tokenToObject, which directly appends the read */
	/* Object to the Array/Object, if it is appropriate. */
	while (1) {
		int rv;

		/* Check the current Token for any kind of JSONObject. */
		currentToken = array_get(tokens, index+startIndex);
		if ((rv = tokenToObject(tokens, index+startIndex, NULL, arr)) == -1) {
			printf("Could not objectify!\n");
			freeObject(arr);
			return -1;
		}
		index += rv;

		/* Mark the Token as used to prevent clearing afterwards. */
		currentToken->tk_type = JSTK_USED;

		/* Check if the current Token is either a closing Bracket or a Comma */
		/* If it is not either of those, free the Array, as there appears to */
		/* be an Error with the Grammar. */
		currentToken = array_get(tokens, index+startIndex);
		if (currentToken->tk_type == JSTK_ARRCLOSE) {
			++index;
			break; // closing Bracket means End

		} else if (currentToken->tk_type == JSTK_COMMA) {
			++index;
			continue; // Comma means more Elements
		}

		/* The Array will be free'd because there is a fatal Error in the */
		/* Grammar. All ressources will be cleared. */
		freeObject(arr);
		return -1;
	}

	*arr_out = arr;
	return index;
}

JSONObject * json_parseFile(const char * filepath) {

	FILE *       file = fopen(filepath, "r");
	stream *     stream;
	JSONObject * obj;

	if (file == NULL) {
		printf("ff\n");
		return NULL;
	}

	stream = stream_create(1024);
	stream_setIn(stream, (uint64_t)file, readFile);

	obj = json_parseStream(stream);

	if (obj == NULL) {
		printf("???? lmao\n");
	}

	stream_free(stream);
	fclose(file);
	return obj;
}

JSONObject * json_parse(const char * str) {

	buffer *     strBuffer = buffer_create(BUFFER_MODE_TAPE);
	stream *     stream    = stream_create(1024);
	JSONObject * obj;

	buffer_write(strBuffer, (uint8_t *) str, strlen(str)+1);
	stream_setIn(stream, (uint64_t)strBuffer, stream_fromBuffer);

	obj = json_parseStream(stream);

	buffer_free(strBuffer);
	stream_free(stream);

	return obj;
}

JSONObject * json_parseStream(stream * s) {

	buffer *      strBuffer    = buffer_create(BUFFER_MODE_TAPE);
	array *       tokens       = array_create(sizeof(jsontoken_t));
	jsontoken_t * currentToken;

	stream_setOut(s, (uint64_t) strBuffer, stream_toBuffer);
	stream_transmit(s, NULL);

	/* Make Tokens from Input, which will be used to parse the JSON */
	/* afterwards. */

	if (tokenizer(strBuffer, s, tokens) == -1) {
		printf("could not tokenize.\n");
		goto cleanup;
	}

	/* Check Entrypoint. The Object may be an Object or Array */
	/* Anything else must be discarded. */

	currentToken = array_get(tokens, 0);
	if (currentToken->tk_type == JSTK_OBJOPEN ||
			currentToken->tk_type == JSTK_ARROPEN) {
		/* Token indicates Object or Array. */

		JSONObject * jObject = NULL;
		int          rv      = 0;

		switch (currentToken->tk_type) {
			case JSTK_OBJOPEN:
				rv = parseObject(tokens, 0, &jObject);
				break;
			case JSTK_ARROPEN:
				rv = parseArray(tokens, 0, &jObject);
				break;
			default:
				break;
		}
		if (rv == -1) {
			printf("could not parse object properly.\n");
			goto cleanup;
		}

		/* Unused Tokens must be cleared inorder to prevent memory leaks. */
		/* The Tokens-Array and the Buffer to which the Input was written */
		/* must be cleared aswell. */
		cleanupTokens(tokens);
		array_free(tokens);
		buffer_free(strBuffer);

		return jObject;
	}

	printf("Could not find object or array.\n");

	cleanup:

	cleanupTokens(tokens);
	array_free(tokens);
	buffer_free(strBuffer);
	return NULL;
}

void json_free(JSONObject * obj) {
	freeObject(obj);
}
