#include "json.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define JSON_LOG(str, rest) fprintf(stdout, "ERR at %s:%d: %s\nString with is following:\n\n%.200s\n\n", __FILE__, __LINE__, str, rest);

static void retrieveValue(JSONObject * object, JSONValue * valueOut) {

	valueOut->val_isNull = 0;
	switch (object->obj_type) {
		case JSON_ARRAY:
		case JSON_OBJECT:
			valueOut->val_object = object;
			break;
		case JSON_STRING:
			valueOut->val_string = object->obj_valueString;
			break;
		case JSON_LONG:
			valueOut->val_long = object->obj_valueLong;
			break;
		case JSON_DOUBLE:
			valueOut->val_double = object->obj_valueDouble;
			break;
		case JSON_BOOL:
			valueOut->val_bool = object->obj_valueBool;
			break;
		case JSON_NULL:
			valueOut->val_isNull = 1;
			break;
	}
}

static JSONObject * getChild(JSONObject * parent, const char * name, int * indexOut) {
	JSONObject * child;
	JSONObject   proxy = {.obj_identifier = (char*)name};
	int          index = array_find(parent->obj_children, &proxy);

	if (index < 0) {
		return NULL;
	}
	if (indexOut != NULL) {
		*indexOut = index;
	}

	child = array_get(parent->obj_children, index);
	return child;
}

JSONObject * jsonobject_create() {

	JSONObject * obj = malloc(sizeof(JSONObject));

	if (obj == NULL) {
		return NULL;
	}

	obj->obj_type       = JSON_OBJECT;
	obj->obj_children   = array_create(0);
	obj->obj_identifier = NULL;

	array_setCompMode(obj->obj_children, json_compare);

	return obj;
}

int jsonobject_add(JSONObject * parent, const char * name, JSONValue value, JSONType type) {
	if (parent != NULL) {
		JSONObject * childObject = NULL;

		if (type == JSON_OBJECT || type == JSON_ARRAY) {
			jsonobject_setName(value.val_object, name);
			array_addSorted(parent->obj_children, value.val_object);
			return 0;
		}

		childObject = jsonobject_create();
        array_free(childObject->obj_children);
		childObject->obj_children = NULL;
		childObject->obj_type     = type;
		jsonobject_setName(childObject, name);
		array_addSorted(parent->obj_children, childObject);

		switch (type) {
			case JSON_LONG:
				childObject->obj_valueLong = value.val_long;
				break;
			case JSON_DOUBLE:
				childObject->obj_valueDouble = value.val_double;
				break;
			case JSON_STRING:
				childObject->obj_valueString = malloc(strlen(value.val_string)+1);
				strcpy(childObject->obj_valueString, value.val_string);
				break;
			case JSON_BOOL:
				childObject->obj_valueBool = value.val_bool;
				break;
			default:
				break;
		}

		return 0;
	}
	return -1;
}

int jsonobject_getByName(JSONObject * parent, const char * name, JSONValue * valueOut) {
	if (parent != NULL && name != NULL) {
		int          index;
		JSONObject * child = getChild(parent, name, &index);

		if (child == NULL) {
			return -1;
		}

		retrieveValue(child, valueOut);
		return 0;
	}
	return -1;
}

int jsonobject_getByName2(JSONObject * obj, const char * name, void * out, JSONType type) {
    JSONValue value;

    if (jsonobject_getByName(obj, name, &value) == -1) {
        return -1;
    }

    switch (type) {
        case JSON_OBJECT:
        case JSON_ARRAY:
            memcpy(out, &value.val_object, sizeof(JSONObject *));
            break;
        case JSON_LONG:
            memcpy(out, &value.val_long, sizeof(long));
            break;
        case JSON_DOUBLE:
            memcpy(out, &value.val_double, sizeof(double));
            break;
        case JSON_STRING:
            memcpy(out, &value.val_string, sizeof(char *));
            break;
        case JSON_BOOL:
            memcpy(out, &value.val_bool, sizeof(bool));
            break;
        default:
            return -1;
    }
    return 0;
}

int jsonobject_getByNames(JSONObject * parent, const char ** names, JSONValue valuesOut[], int n) {
	for (int i = 0; i < n; ++i) {
		if (jsonobject_getByName(parent, names[i], &valuesOut[i]) == -1) {
			return i;
		}
	}
	return -1;
}

int jsonobject_getAt(JSONObject * parent, int index, JSONValue * valueOut) {
	if (parent != NULL && index >= 0) {
		JSONObject * object = array_get(parent->obj_children, index);

		if (object != NULL) {
			retrieveValue(object, valueOut);
			return 0;
		}
	}
	return -1;
}

int jsonobject_getPropertycount(JSONObject * object) {
	if (object != NULL) {
		return array_getLength(object->obj_children);
	}
	return -1;
}

int jsonobject_remove(JSONObject * parent, const char * name) {
	if (parent != NULL && name != NULL) {
		int index;

		getChild(parent, name, &index);
		if (index < 0) {
			return -1;
		}

		array_remove(parent->obj_children, index);
		return 0;
	}
	return -1;
}

int jsonobject_removeAt(JSONObject * parent, int index) {
	if (parent != NULL && index >= 0) {
		array_remove(parent->obj_children, index);
		return 0;
	}
	return -1;
}

const char * jsonobject_getPropertynameAt(JSONObject * object, int index) {
	if (object != NULL && index >= 0) {
		JSONObject * child = array_get(object->obj_children, index);

		if (child == NULL) {
			return NULL;
		}
		return (const char *) (child->obj_identifier);
	}
	return NULL;
}

int jsonobject_setName(JSONObject * obj, const char * name) {
	if (obj != NULL && name != NULL) {
		if (obj->obj_identifier != NULL) {
			free(obj->obj_identifier);
		}

		obj->obj_identifier = malloc(strlen(name)+1);
		strcpy(obj->obj_identifier, name);
		return 0;
	}
	return -1;
}

int jsonobject_rename(JSONObject * parent, const char * childname, const char * newname) {
	if (parent != NULL && childname != NULL && newname != NULL) {
		int          index;
		JSONObject * child = getChild(parent, childname, &index);

		return jsonobject_setName(child, newname);
	}
	return -1;
}

JSONArray * jsonarray_create() {
	JSONArray * arr = malloc(sizeof(JSONObject));

	if (arr == NULL) {
		return NULL;
	}

	arr->obj_type       = JSON_ARRAY;
	arr->obj_children   = array_create(0);
	arr->obj_identifier = NULL;

	return arr;
}

int jsonarray_set(JSONArray * arr, int index, JSONValue value, JSONType type) {
	if (arr != NULL) {

		JSONObject * child = NULL;

		if (type == JSON_OBJECT || type == JSON_ARRAY) {
			child = value.val_object;
		} else {
			child = jsonobject_create();

			child->obj_children   = NULL;
			child->obj_identifier = NULL;
			child->obj_type       = type;

			switch (type) {
				case JSON_LONG:
					child->obj_valueLong = value.val_long;
					break;
				case JSON_DOUBLE:
					child->obj_valueDouble = value.val_double;
					break;
				case JSON_STRING:
					child->obj_valueString = malloc(strlen(value.val_string)+1);
					strcpy(child->obj_valueString, value.val_string);
					break;
				case JSON_BOOL:
					child->obj_valueBool = value.val_bool;
					break;
				default:
					break;
			}
		}

		if (index < 0) {
			array_append(arr->obj_children, child);
		} else {
			array_insertAt(arr->obj_children, index, child);
		}
		return 0;
	}
	return -1;
}

int jsonarray_get(JSONArray * arr, int index, JSONValue * valueOut) {
	if (arr != NULL && index >= 0 && valueOut != NULL) {
		JSONObject * element = array_get(arr->obj_children, index);

		if (element == NULL) {
			return -1;
		}
		retrieveValue(element, valueOut);
        return 0;
	}
	return -1;
}

int jsonarray_get2(JSONArray * arr, int index, void * out, JSONType type) {
    JSONValue value;

    if (jsonarray_get(arr, index, &value) == -1) {
        return -1;
    }

    switch (type) {
        case JSON_OBJECT:
        case JSON_ARRAY:
            memcpy(out, &value.val_object, sizeof(JSONObject*));
            break;
        case JSON_LONG:
            memcpy(out, &value.val_long, sizeof(long));
            break;
        case JSON_DOUBLE:
            memcpy(out, &value.val_double, sizeof(double));
            break;
        case JSON_STRING:
            memcpy(out, &value.val_string, sizeof(char *));
            break;
        case JSON_BOOL:
            memcpy(out, &value.val_bool, sizeof(bool));
            break;
        default:
            return -1;
    }
    return 0;
}

int jsonarray_removeAt(JSONArray * arr, int index) {
	if (arr != NULL && index >= 0) {
		array_remove(arr->obj_children, index);
		return 0;
	}
	return -1;
}

int jsonarray_getLength(JSONArray * arr) {
	return jsonobject_getPropertycount(arr);
}

int json_compare(void * obj1, void * obj2) {
	if (obj1 != NULL && obj2 != NULL) {
		char * str1 = ((JSONObject *)obj1)->obj_identifier;
		char * str2 = ((JSONObject *)obj2)->obj_identifier;

		for (int i = 0; 1; ++i) {
			char c1 = str1[i];
			char c2 = str2[i];

			if (c1 == '\0' && c2 != '\0') {
				return -1;
			} else if (c2 == '\0' && c1 != '\0') {
				return 1;
			} else if (c1 == '\0' && c2 == '\0') {
				return 0;
			}

			if (c1 < c2) {
				return -1;
			} else if (c1 > c2) {
				return 1;
			}
		}
		return 0;
	}
	return -2;
}
