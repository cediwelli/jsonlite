#include <stdio.h>
#include <stdlib.h>
#include <jsonparse/json.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#include <datastructures/stream.h>

uint32_t readfile(uint64_t fd, uint8_t * buf, uint32_t n) {
	int actual_fd = (int) fd;
	int r         = 0;

	r  = read(actual_fd, buf, n);
	return (uint32_t)r;
}

int main(int argc, char ** argv) {

    JSONObject* obj = json_parseFile("test.json");
    char* text = NULL;

    jsonobject_getByName2(obj, "2duisburg", &text, JSON_STRING);

    printf("peter: %s \n", text);

    return 0;
}
