CFLAGS=-Wall -pedantic -Werror

FILES=$(wildcard src/*.c)
OBJECTS=$(patsubst src/%.c,bin/%.o,$(FILES))
INCLUDE=-Iinclude/

LIBNAME=jsonparse
LIBOUT=bin/lib$(LIBNAME).so

LIBS=-Llibs/ -ldatastructures

all: $(OBJECTS)
	gcc -g3 $(OBJECTS) $(CFLAGS) $(LIBS) -fPIC -shared -o $(LIBOUT)
	chmod 644 $(LIBOUT)

bin/%.o: src/%.c
	gcc -c -g3 -fPIC $< $(CFLAGS) $(INCLUDE) -o $@

clean:
	rm bin/*

install:
	mkdir /usr/include/$(LIBNAME)
	cp $(wildcard include/*) /usr/include/$(LIBNAME)
	cp $(LIBOUT) /usr/lib/

remove:
	rm -r /usr/include/$(LIBNAME)
	rm /usr/lib/lib$(LIBNAME).so
