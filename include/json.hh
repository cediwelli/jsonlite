#pragma once

#include <string>
#include "json.h"

class JsonObject;
typedef JsonObject JsonArray;

class JsonObject {
    private:
        JSONObject* jsonObject;

    public:
        JsonObject() {
            this->jsonObject = nullptr;
        }

        void free() {
            json_free(this->jsonObject);
            this->jsonObject = nullptr;
        }

        int getPropertyCount() {
            return jsonobject_getPropertycount(this->jsonObject);
        }

        std::string getPropertyNameAt(int index) {
            std::string name(jsonobject_getPropertynameAt(this->jsonObject, index));
            return name;
        }

        void get(std::string name, long& out) {
            jsonobject_getByName2(this->jsonObject, name.c_str(), &out, JSON_LONG);
        }

        void get(std::string name, double& out) {
            jsonobject_getByName2(this->jsonObject, name.c_str(), &out, JSON_DOUBLE);
        }

        void get(std::string name, std::string& out) {
            const char* cstr_val;
            jsonobject_getByName2(this->jsonObject, name.c_str(), &cstr_val, JSON_STRING);
            out = std::string(cstr_val);
        }

        void get(std::string name, bool& out) {
            jsonobject_getByName2(this->jsonObject, name.c_str(), &out, JSON_BOOL);
        }

        void get(std::string name, JsonObject& out) {
            jsonobject_getByName2(this->jsonObject, name.c_str(), &(out.jsonObject), JSON_OBJECT);
        }

        void add(std::string name, long in) {
            JSONValue val;
            val.val_long = in;
            jsonobject_add(this->jsonObject, name.c_str(), val, JSON_LONG);
        }

        void add(std::string name, double in) {
            JSONValue val;
            val.val_double = in;
            jsonobject_add(this->jsonObject, name.c_str(), val, JSON_DOUBLE);
        }

        void add(std::string name, std::string in) {
            JSONValue val;
            val.val_string = in.c_str();
            jsonobject_add(this->jsonObject, name.c_str(), val, JSON_STRING);
        }

        void add(std::string name, bool in) {
            JSONValue val;
            val.val_bool = in;
            jsonobject_add(this->jsonObject, name.c_str(), val, JSON_BOOL);
        }

        void add(std::string name, JsonObject in) {
            JSONValue val;
            val.val_object = in.jsonObject;
            jsonobject_add(this->jsonObject, name.c_str(), val, JSON_OBJECT);
        }

        void remove(std::string name) {
            jsonobject_remove(this->jsonObject, name.c_str());
        }

        void get(int index, long& out) {
            jsonarray_get2(this->jsonObject, index, &out, JSON_LONG);
        }

        void get(int index, double& out) {
            jsonarray_get2(this->jsonObject, index, &out, JSON_DOUBLE);
        }

        void get(int index, std::string& out) {
            const char* cstr_val;
            jsonarray_get2(this->jsonObject, index, &cstr_val, JSON_STRING);
            out = std::string(cstr_val);
        }

        void get(int index, bool& out) {
            jsonarray_get2(this->jsonObject, index, &out, JSON_BOOL);
        }

        void get(int index, JsonObject& out) {
            jsonarray_get2(this->jsonObject, index, &(out.jsonObject), JSON_OBJECT);
        }

        void add(int index, long in) {
            JSONValue val;
            val.val_long = in;
            jsonarray_set(this->jsonObject, index, val, JSON_LONG);
        }

        void add(int index, double in) {
            JSONValue val;
            val.val_double = in;
            jsonarray_set(this->jsonObject, index, val, JSON_DOUBLE);
        }

        void add(int index, std::string in) {
            JSONValue val;
            val.val_string = in.c_str();
            jsonarray_set(this->jsonObject, index, val, JSON_STRING);
        }

        void add(int index, bool in) {
            JSONValue val;
            val.val_bool = in;
            jsonarray_set(this->jsonObject, index, val, JSON_BOOL);
        }

        void add(int index, JsonObject in) {
            JSONValue val;
            val.val_object = in.jsonObject;
            jsonarray_set(this->jsonObject, index, val, JSON_OBJECT);
        }

        void remove(int index) {
            jsonarray_removeAt(this->jsonObject, index);
        }

        int length() {
            return jsonarray_getLength(this->jsonObject);
        }

        static JsonObject parseFile(std::string file) {
            JsonObject jobj;
            jobj.jsonObject = json_parseFile(file.c_str());
            return jobj;
        }

        static JsonObject parse(std::string str) {
            JsonObject jobj;
            jobj.jsonObject = json_parse(str.c_str());
            return jobj;
        }
};
