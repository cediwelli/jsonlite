#ifndef JSON_H_
#define JSON_H_

#include <stdint.h>
#include <stdbool.h>
#include <datastructures/stream.h>
#include <datastructures/array.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @defgroup JSONLIB
 * */

#define JSON_MAX_STRING_LENGTH 8192

/**
 * @ingroup JSONLIB
 * @brief
 * The JSONObject is the generic Type of the JSONObject and JSONArray. A
 * JSONArray may be returned by a function as a JSONObject*.
 * */
typedef struct {
	uint8_t obj_type;
	char * obj_identifier;

	union {
		array *	obj_children;
		long	obj_valueLong;
		double	obj_valueDouble;
		char *	obj_valueString;
		bool	obj_valueBool;
	};
} JSONObject;

/**
 * @ingroup JSONLIB
 * @brief
 * This union can hold all possible Values.
 * */
typedef union {
	JSONObject * val_object;
	long         val_long;
	double       val_double;
	bool         val_bool;
	const char * val_string;
	int          val_isNull;
} JSONValue;

typedef JSONObject JSONArray;

/**
 * @ingroup JSONLIB
 * @brief
 * These are the possible types a Value can be.
 * */
typedef enum {
	JSON_LONG, JSON_DOUBLE, JSON_STRING, JSON_OBJECT, JSON_ARRAY, JSON_BOOL, JSON_NULL
} JSONType;

/**
 * @ingroup JSONLIB
 * @brief
 * Parse JSON from a String. The resulting Object may be an JSONObject or
 * JSONArray.
 *
 * @param[in] str The String from which the JSON will be parsed.
 * @return Returns either a JSONObject, JSONArray, or NULL on Error.
 * */
JSONObject * json_parse(const char * str);

/**
 * @ingroup JSONLIB
 * @brief
 * Parse JSON from a File. The file will be read and parsed automatically.
 * The resulting Object may be an JSONObject or JSONArray.
 *
 * @param[in] filepath The path of the File.
 * @return Returns either a JSONObject, JSONArray, or NULL on Error.
 * */
JSONObject * json_parseFile(const char * filepath);

/**
 * @ingroup JSONLIB
 * @brief
 * Parse JSON from a Stream.
 *
 * @param[in] s The Stream from which will be read.
 * @return Returns either a JSONObject, JSONArray, or NULL on Error.
 * */
JSONObject * json_parseStream(stream * s);

/**
 * @ingroup JSONLIB
 * @brief
 * Creates a new JSONObject. These Objects may be used to nest other Objects or
 * Values in Key-Value-Pairs inside of them.
 *
 * @return Returns the created Object or NULL on Error.
 * */
JSONObject * jsonobject_create();

/**
 * @ingroup JSONLIB
 * @brief
 * Frees the allocated memory of any Object/Array.
 *
 * @param[in] obj The Object to free.
 * */
void json_free(JSONObject * obj);

/**
 * @ingroup JSONLIB
 * @brief
 * Add a Key-Value-Pair to an Object. The Value can be later retrieved with
 * {@link jsonobject_getByName} or {@link jsonobject_getAt}.
 *
 * @param[in] parent The Parent-Object to which the Key-Value-Pair will be added.
 * @param[in] name The Name (Key) of the Key-Value-Pair.
 * @param[in] value The {@link JSONValue Value} of the Key-Value-Pair.
 * @param[in] type The Type of Data that value holds.
 * @return Returns 0 on success or -1 on Error.
 * */
int jsonobject_add(JSONObject * parent, const char * name, JSONValue value, JSONType type);

/**
 * @ingroup JSONLIB
 * @brief
 * Get the Value which is identified by the Name. This will search the Object
 * for the provided Name and retrieve its Data. The Value will be written to
 * valueOut.
 *
 * @param[in] obj The Object to retrieve a Value from.
 * @param[in] name The Name of the Key-Value-Pair.
 * @param[out] valueOut Provide a {@link JSONValue} structure to which the Value
 * 				will be written.
 * @return Returns 0 on Success or -1 on Error.
 * */
int jsonobject_getByName(JSONObject * obj, const char * name, JSONValue * valueOut);

/**
 * @ingroup JSONLIB
 * @brief
 * Get the Value which is identified by the Name. This will search the Object
 * for the provided Name and retrieve its Data. The Value will be written to
 * out. Examples:
 *
 * @code
 * JSONObject * obj = json_parse(...);
 * long        integerValue;
 * double      doubleValue;
 * JSONObject* objectValue;
 * char*       stringValue;
 *
 * jsonobject_getByName2(obj, "example_ival", &integerValue, JSON_LONG);
 * jsonobject_getByName2(obj, "example_dval", &doubleValue, JSON_DOUBLE);
 * jsonobject_getByName2(obj, "example_oval", &objectValue, JSON_OBJECT);
 * jsonobject_getByName2(obj, "example_sval", &stringValue, JSON_STRING);
 * @endcode
 *
 * @param[in] obj The Object to retrieve a Value from.
 * @param[in] name The Name of the Key-Value-Pair.
 * @param[out] out A Pointer to which the retrieved Value will be written.
 * @param[in] type The Type of the Value.
 * @return Returns 0 on Success or -1 on Error.
 * */
int jsonobject_getByName2(JSONObject * obj, const char * name, void * out, JSONType type);

/**
 * @ingroup JSONLIB
 * @brief
 * Get the Values which are identified by the passed Names. This is a convinience
 * function, so that multiple Values can be retrieved by a single function call.
 *
 * @param[in] obj The Parent Object from which the Values will be retreived.
 * @param[in] names An Array of strings with the names.
 * @param[out] valuesOut an Array of JSONValues to which the results will be
 * 			written.
 * @param[in] n The amount of Names to search for. Both names and valuesOut must
 * 			be of the length of n.
 * @return Returns -1 on success or the index of the violating name on Error.
 * */
int jsonobject_getByNames(JSONObject * obj, const char ** names,
		JSONValue valuesOut[], int n);

/**
 * @ingroup JSONLIB
 * @brief
 * Get the Value at the provided Index, if it exists. The Value will be written
 * to the structure pointed to by valueOut. The user has to provide the memory.
 *
 * @param[in] obj The Object to retrieve a Value from.
 * @param[in] index Retrieve the Child at that Index.
 * @param[out] valueOut To this memory-location, the Value will be written.
 * @return Returns 0 on Success or -1 on Error.
 * */
int jsonobject_getAt(JSONObject * obj, int index, JSONValue * valueOut);

/**
 * @ingroup JSONLIB
 * @brief
 * Get the amount of Children/Proerties of an Object.
 *
 * @param[in] obj The Object from which the count of Children will be returned.
 * @return Returns the amount of Children or -1 on Error.
 * */
int jsonobject_getPropertycount(JSONObject * obj);

/**
 * @ingroup JSONLIB
 * @brief
 * Remove a Child of an Object by providing the Name of the Child. The Child
 * will be fully removed from the Parent. This operation is irreversable.
 *
 * @param[in] obj The Parent-Object from which the Child will be removed.
 * @param[in] name The Name of the Child.
 * @return Returns -1 on Error or 0 either on Success or if no Child with this
 * 			Name was found.
 * */
int jsonobject_remove(JSONObject * obj, const char * name);

/**
 * @ingroup JSONLIB
 * @brief
 * Remove a Child of an Object by providing the Index of the Child. The Child
 * will be fully removed from the Parent. This operation is irreversable.
 *
 * @param[in] obj The Parent-Object from which the Child will be removed.
 * @param[in] index The Index at which the Child shall be removed.
 * @return Returns -1 on Error or 0 either on Success or if no Child with this
 * 			Index was found.
 * */
int jsonobject_removeAt(JSONObject * obj, int index);

/**
 * @ingroup JSONLIB
 * @brief
 * Get the Name of an Object inside the Parent-Object obj at the given Index.
 * The Value of the Name MUST NOT be changed! To rename a Property, use
 * {@link jsonobject_rename}.
 *
 * @param[in] obj The Parent-Object from which a propertyname of a Child will be
 * 				retrieved.
 * @param[in] index The Index of the Child from which the propertyname will be
 * 				retrieved.
 * @return Returns 0 on Success or -1 on Error.
 * */
const char * jsonobject_getPropertynameAt(JSONObject * obj, int index);

/**
 * @ingroup JSONLIB
 * @brief
 * Renames a Child of an Object.
 *
 * @param[in] parent The Parent-Object of which a Child will be renamed.
 * @param[in] childname The current Name of the Child.
 * @param[in] newname The Name to which the Child will be renamed.
 * @return Returns 0 on Success or -1 on Error.
 * */
int jsonobject_rename(JSONObject * parent, const char * childname, const char * newname);

/**
 * @ingroup JSONLIB
 * @brief
 * Will set the Name of an JSONObject or JSONArray. This function is quite
 * useless to the user as the Name will be set on adding the Object to a
 * Parent. There is no usecase in which the user should use this function.
 *
 * @param[in] obj The Object of which the Name will be set.
 * @param[in] name The Name that will be assigned to the Object.
 * @return Returns 0 on Success or -1 on Error.
 * @deprecated
 * */
int jsonobject_setName(JSONObject * obj, const char * name);

/**
 * @ingroup JSONLIB
 * @brief
 * Creates a new JSONArray.
 *
 * @return Returns the new JSONArray.
 * */
JSONArray * jsonarray_create();

/**
 * @ingroup JSONLIB
 * @brief
 * Will set a Value at the given Index. This function acts more like an append
 * and insert function, as setting at Indecies greater than zero will cause an
 * insertion (rather than replacing the Value at that Index) and an Index less
 * then zero will cause appending.
 *
 * @param[in] arr The Array to add Value to.
 * @param[in] index The Index at which will be inserted. Use index<0 to indicate
 * 				that the Value should be appended.
 * @param[in] type The type of the Value.
 * @return Returns 0 on Success or -1 on Error.
 * */
int jsonarray_set(JSONArray * arr, int index, JSONValue value, JSONType type);

/**
 * @ingroup JSONLIB
 * @brief
 * Get the Value at a given Index.
 *
 * @param[in] arr The Array from which the Value will be retrieved.
 * @param[in] index The Index at which the Value lies.
 * @param[out] valueOut The memory-location to which the Value will be written.
 * @return Returns 0 on Success or -1 on Error.
 * */
int jsonarray_get(JSONArray * arr, int index, JSONValue * valueOut);

/**
 * @ingroup JSONLIB
 * @brief
 * Get the Value at a given Index. For examples see {@link jsonobject_getByName2}.
 *
 * @param[in] arr The Array from which the Value will be retrieved.
 * @param[in] index The Index at which the Value lies.
 * @param[out] value The memory-location to which the Value will be written.
 * @param[in] type The Type of the Value that will be written.
 * @return Returns 0 on Success or -1 on Error.
 * @see jsonobject_getByName2
 * */
int jsonarray_get2(JSONArray * arr, int index, void * out, JSONType type);

/**
 * @ingroup JSONLIB
 * @brief
 * Remove an Element of the Array at the specified Index.
 *
 * @param[in] arr The Array from which will be removed.
 * @param[in] index The Index at which the Value will be removed.
 * @return Returns 0 on Success or -1 on Error.
 * */
int jsonarray_removeAt(JSONArray * arr, int index);

/**
 * @ingroup JSONLIB
 * @brief
 * Get the Length (count of Elements) of the Array.
 *
 * @param[in] arr The Array to get the count from.
 * @return Returns the length of the Array or -1 on Error.
 * */
int jsonarray_getLength(JSONArray * arr);

/**
 * @ingroup JSONLIB
 * @brief
 * Converts a JSONObject to a String.
 *
 * @param[in] obj A JSONObject.
 * @return Returns a String which is dynamically allocated and must be free'd by
 * 			the user or NULL.
 * */
char * json_stringify(JSONObject * obj);
int json_stringifyToFile(JSONObject * obj, char * filepath);
int json_stringifyToStream(JSONObject * object, stream * s);

int json_stringEscape(char * buffer, char * str);
int json_stringUnescape(char * buffer, char * str);

int json_compare(void * obj1, void * obj2);

#ifdef __cplusplus
}
#endif

#endif
